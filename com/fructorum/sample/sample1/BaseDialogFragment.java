package com.fructorum.sample.sample1;

import android.app.Application;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;


public class BaseDialogFragment  extends DialogFragment {

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }

    public MApplication getApp() {
        return getBaseActivity().getApp();
    }

    public void toast(String string) {
        getBaseActivity().toast(string);
    }

    public void longToast(String string) {
        getBaseActivity().longToast(string);
    }

    public void toast(final @StringRes int text) {
        getBaseActivity().toast(text);
    }

    public void dtoast(String string) {
        if (Config.CONFIG.DTOASTS_ENABLED()) {
            toast(string);
        }
    }

    public void runOnUiThread(Runnable r) {
        BaseActivity activity = getBaseActivity();
        if (activity != null) {
            activity.runOnUiThread(r);
        }
    }
}