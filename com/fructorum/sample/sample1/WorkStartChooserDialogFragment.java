package com.fructorum.sample.sample1;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.fructorum.sample.R;

import java.util.Date;

public class WorkStartChooserDialogFragment extends BaseDialogFragment implements SingleDateChooserFragment.Listener {

    private static final String FTAG_START_DATE_CHOOSER = "FTAG_START_DATE_CHOOSER";

    private TextView dateTo;

    private Date dateChoosen;

    public static WorkStartChooserDialogFragment newInstance() {
        return new WorkStartChooserDialogFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.mDialogTheme);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.work_start_chooser_dialog, container, false);

        TextView chooseDateFrom = (TextView) view.findViewById(R.id.choose_date_from);
        chooseDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingleDateChooserFragment datesChooserFragment = SingleDateChooserFragment.newInstance(null);
                datesChooserFragment.show(getChildFragmentManager(), FTAG_START_DATE_CHOOSER);
            }
        });

        Button buttonOk = (Button) view.findViewById(R.id.ok);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean valide = validateChooseStartDate();
                if (valide) {
                    ((Listener) getParentFragment()).onDateChoosen(getTag(), dateChoosen);
                    dismiss();
                } else {
                    toast(R.string.validation_work_start_dates);
                }
            }
        });

        Button buttonCancel = (Button) view.findViewById(R.id.cancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        dateTo = (TextView) view.findViewById(R.id.from_date);
        return view;
    }


    @Override
    public void onDateChoosen(String ftag, Date date) {
        this.dateChoosen = date;
        dateTo.setText(StringUtils.convertDatetoString(date));
    }

    private boolean validateChooseStartDate() {
        boolean valide = true;

        if (dateChoosen != null) {
            return valide;
        }
        return false;
    }


    public interface Listener {

        public void onDateChoosen(String ftag, Date date);

    }
}