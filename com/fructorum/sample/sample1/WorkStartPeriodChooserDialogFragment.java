package com.fructorum.sample.sample1;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.fructorum.sample.R;

import java.util.Date;

public class WorkStartPeriodChooserDialogFragment extends BaseDialogFragment implements SingleDateChooserFragment.Listener {

    private static final String FTAG_START = "FTAG_WORK_START_PERIOD_CHOOSER";
    private static final String FTAG_FINISH = "FTAG_WORK_FINISH_PERIOD_CHOOSER";

    private TextView txtFromDate;
    private TextView txtToDate;

    private Date dateChoosenFrom;
    private Date dateChoosenTo;


    public static WorkStartPeriodChooserDialogFragment newInstance() {
        return new WorkStartPeriodChooserDialogFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.mDialogTheme);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.work_start_choose_period_dialog, container, false);

        final TextView chooseDateFrom = (TextView) view.findViewById(R.id.choose_date_from);
        final TextView chooseDateTo = (TextView) view.findViewById(R.id.choose_date_to);

        txtFromDate = (TextView) view.findViewById(R.id.from_date);
        txtToDate = (TextView) view.findViewById(R.id.to_date);

        Button buttonOk = (Button) view.findViewById(R.id.ok);
        Button buttonCancel = (Button) view.findViewById(R.id.cancel);

        chooseDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingleDateChooserFragment datesChooserFragment = SingleDateChooserFragment.newInstance(null);
                datesChooserFragment.show(getChildFragmentManager(), FTAG_START);
            }
        });


        chooseDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SingleDateChooserFragment datesChooserFragment = SingleDateChooserFragment.newInstance(null);
                datesChooserFragment.show(getChildFragmentManager(), FTAG_FINISH);
            }
        });


        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean valide = validateDate();
                if (valide) {
                    ((Listener) getParentFragment()).onDateChoosen(getTag(), dateChoosenFrom, dateChoosenTo);
                    dismiss();
                }
            }
        });


        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }


    @Override
    public void onDateChoosen(String ftag, Date date) {

        if (ftag.equals(FTAG_FINISH)) {
            dateChoosenTo = date;
            txtToDate.setText(StringUtils.convertDatetoString(date));
        } else {
            dateChoosenFrom = date;
            txtFromDate.setText(StringUtils.convertDatetoString(date));

        }
    }

    private boolean validateChoosentDateNotEmpty() {
        boolean valide = true;

        if (dateChoosenTo != null && dateChoosenFrom != null) {
            return valide;
        }
        return false;
    }

    private boolean validateDate() {
        if (validateChoosentDateNotEmpty()) {
            if (dateChoosenTo.getTime() < dateChoosenFrom.getTime()) {
                toast(getString(R.string.valid_error_date_add_order));
                return false;
            } else {
                return true;
            }
        } else {
            toast(getString(R.string.validation_work_start_dates));
            return false;
        }

    }

    public interface Listener {

        public void onDateChoosen(String ftag, Date from, Date to);

    }
}
