package com.fructorum.sample.sample1;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.fructorum.sample.R;
import com.fructorum.sample.sample1.BaseDialogFragment;
import com.fructorum.sample.sample1.WorkStartChooserDialogFragment;
import com.fructorum.sample.sample1.WorkStartPeriodChooserDialogFragment;

import java.util.Date;

public class WorkStartOptionChooserDialogFragment  extends BaseDialogFragment implements WorkStartChooserDialogFragment.Listener,
        WorkStartPeriodChooserDialogFragment.Listener {

    private final String FTAG_START_CHOOSER = "FTAG_START_CHOOSER";
    private final String FTAG_PERIOD_CHOOSER = "FTAG_PERIOD_CHOOSER";

    private RadioGroup workStartRadioGroup;

    private Date dateFrom;
    private Date dateTo;

    public static final int CHOOSE_ANY = 1;
    public static final int CHOOSE_PERIOD = 2;
    public static final int CHOOSE_START = 3;


    private int chooseType;

    public static WorkStartOptionChooserDialogFragment newInstance() {
        return new WorkStartOptionChooserDialogFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.mDialogTheme);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.work_start_option_chooser_dialog, container, false);

        workStartRadioGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        workStartRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.any_time:
                        chooseType = CHOOSE_ANY;
                        setDateAndCloseDialog();
                        break;
                    case R.id.during_period:
                        WorkStartPeriodChooserDialogFragment workStartPeriodChooserDialogFragment = WorkStartPeriodChooserDialogFragment.newInstance();
                        workStartPeriodChooserDialogFragment.show(getChildFragmentManager(), FTAG_START_CHOOSER);
                        break;
                    case R.id.no_later_than:
                        WorkStartChooserDialogFragment workStartChooserFragment = WorkStartChooserDialogFragment.newInstance();
                        workStartChooserFragment.show(getChildFragmentManager(), FTAG_PERIOD_CHOOSER);
                        break;
                }
            }
        });

        return view;
    }


    @Override
    public void onDateChoosen(String ftag, Date date) {
        dateFrom = date;
        chooseType = CHOOSE_START;
        setDateAndCloseDialog();
    }

    @Override
    public void onDateChoosen(String ftag, Date from, Date to) {
        dateFrom = from;
        dateTo = to;
        chooseType = CHOOSE_PERIOD;
        setDateAndCloseDialog();
    }


    private void setDateAndCloseDialog() {
        ((Listener) getActivity()).onWorkStartOptionChoosen(getTag(),
                chooseType,
                dateFrom,
                dateTo);
        dismiss();
    }
    public interface Listener {

        public void onWorkStartOptionChoosen(String ftag, Integer option, Date from, Date to);

    }
}
