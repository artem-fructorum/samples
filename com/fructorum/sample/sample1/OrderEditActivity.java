package com.fructorum.sample.sample1;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.AppCompatSeekBar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fructorum.sample.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class OrderEditActivity extends BaseActivity implements WorkStartOptionChooserDialogFragment.Listener,
        AttachFileDeleteDialogFragment.Listener {

    private static final String TAG = "OrderEditActivity";

    private TextView termsConditionsLink;
    private TextView howToFillOrderInfoLink;
    private TextView workStartOptionsChooser;

    private Button btnUploadAttachFile;

    private Date selectedDateStart = null;
    private Date selectedDateFinish = null;

    private final String HOW_TO_FILL_ADDITIONAL_INFO_URL = "file:///android_asset/xxxxx";
    private final String TERMS_CONDITIONS_URL = "https:xxxx";
    public static final String EXTRA_ORDER_ID = "EXTRA_ORDER_ID";

    public static final String FTAG_HOW_TO_FILL_ORDER_INFO = "FTAG_HOW_TO_FILL_ORDER_INFO";
    public static final String FTAG_TERMS_CONDITIONS_ORDER = "FTAG_TERMS_CONDITIONS_ORDER";
    public static final String FTAG_DATE_ORDER = "FTAG_DATE_ORDER";
    public static final String FTAG_DELETE_ATTACH_FILE = "FTAG_DELETE_ATTACH_FILE";

    private final int PICK_IMAGE_REQUEST = 1;

    private GridView gridView;
    private ImagesAdapter imagesAdapter;
    private LayoutInflater inflater;

    private List<Uri> pictures = new ArrayList<>();

    private TextView locationNameTextView;
    private Settings settings;

    private AppCompatSeekBar seekBarBudget;
    private List<BudgetCategoryItem> budgetCategories = new ArrayList<>();
    private TextView orderBudget;
    private TextView orderBudgetDescription;

    private LinearLayout unregisteredFieldsPart;

    private boolean isLoggedIn;
    private EditText orderTitle;
    private EditText orderDescription;
    private EditText orderContactInfo;
    private EditText orderEmail;
    private EditText orderPhone;

    private ScrollView scrollview;
    private Handler handler;

    private Button btnSendOffer;
    private CheckBox termsConditional;

    private String locationName = null;
    private String locationLatitude;
    private String locationLongitude;

    private Long choosenBudgetCategoryId = null;
    private long startDateOption = None.NONE_LONG;

    private MClient mClient;
    private Bus bus;
    private OrderMixedItem orderMixedItem;
    private long orderMixedId = None.NONE_LONG;
    private mDatabase db;

    private ProgressBar progressBar;
    private View retryPart;
    private Button retryButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_edit_activity);

        db = getApp().getDatabase();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            orderMixedId = bundle.getLong(EXTRA_ORDER_ID, None.NONE_LONG);
            if (orderMixedId != None.NONE_LONG) getSupportActionBar().setTitle(getString(R.string.update_order_title));
        }

        progressBar = (ProgressBar) findViewById(R.id.loading_progress);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminate(true);

        settings = getApp().getSettings();

        bus = getApp().getBus();
        bus.register(this);
        mClient = getApp().getClient();

        mClient.getBudgetCategoriesAsync();

        showOrHideUnregisteredFieldsPart();
        retryPart = findViewById(R.id.retry_part);
        retryButton = (Button) findViewById(R.id.retry_button);
        retryPart.setVisibility(View.GONE);
        retryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                retryPart.setVisibility(View.GONE);
                mClient.getBudgetCategoriesAsync();
            }
        });
        scrollview = (ScrollView) findViewById(R.id.content);
        scrollview.setVisibility(View.GONE);

        termsConditionsLink = (TextView) findViewById(R.id.terms_conditional);
        termsConditionsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showInfoDialog(TERMS_CONDITIONS_URL, FTAG_HOW_TO_FILL_ORDER_INFO);
            }
        });

        termsConditional = (CheckBox) findViewById(R.id.terms_conditional_checkbox);

        howToFillOrderInfoLink = (TextView) findViewById(R.id.order_details_example);
        howToFillOrderInfoLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showInfoDialog(HOW_TO_FILL_ADDITIONAL_INFO_URL, FTAG_TERMS_CONDITIONS_ORDER);
            }
        });

        workStartOptionsChooser = (TextView) findViewById(R.id.work_start_options_chooser);
        workStartOptionsChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WorkStartOptionChooserDialogFragment fragment = WorkStartOptionChooserDialogFragment.newInstance();
                fragment.show(getSupportFragmentManager(), FTAG_DATE_ORDER);
            }
        });

        btnUploadAttachFile = (Button) findViewById(R.id.upload_attach_file);
        btnUploadAttachFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });

        inflater = getLayoutInflater();
        gridView = (GridView) findViewById(R.id.order_images);
        imagesAdapter = new ImagesAdapter(inflater);
        imagesAdapter.setListener(new ImagesAdapter.OnClickListener() {
            @Override
            public void onClick(Uri pictureItem) {
                AttachFileDeleteDialogFragment fragment = AttachFileDeleteDialogFragment.newInstance(pictureItem);
                fragment.show(getSupportFragmentManager(), FTAG_DELETE_ATTACH_FILE);
            }
        });
        gridView.setAdapter(imagesAdapter);


        locationNameTextView = (TextView) findViewById(R.id.order_location);
        locationNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapActivityExt.pickGeoPos(OrderEditActivity.this, getString(R.string.сhoose_location), settings.getLocationGeoPoint());
            }
        });


        orderBudget = (TextView) findViewById(R.id.order_budget);
        orderBudgetDescription = (TextView) findViewById(R.id.order_budget_description);
        seekBarBudget = (AppCompatSeekBar) findViewById(R.id.seekBarBudget);
        disableSeekBarChangeListener();

        orderTitle = (EditText) findViewById(R.id.order_title);
        orderDescription = (EditText) findViewById(R.id.order_details);
        orderContactInfo = (EditText) findViewById(R.id.order_contact_info);
        orderEmail = (EditText) findViewById(R.id.order_email);

        orderPhone = (EditText) findViewById(R.id.order_phone);

        final String PHONE_PREFIX = getString(R.string.phone_prefix_russia);
        orderPhone.addTextChangedListener(new TextWatcher() {

            private String oldValue;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                oldValue = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!editable.toString().startsWith(PHONE_PREFIX)) {
                    orderPhone.setText(oldValue);
                    Selection.setSelection(orderPhone.getText(), orderPhone.getText().length());
                }
            }
        });

        handler = new Handler();

        btnSendOffer = (Button) findViewById(R.id.send_offer_btn);
        btnSendOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOrder();
            }
        });

        if (orderMixedId != None.NONE_LONG) {
            orderMixedItem = db.getOrderMixed(orderMixedId);
            prefill();
        }
    }

    private void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), PICK_IMAGE_REQUEST);
    }

    private void showInfoDialog(String urlInfo, String fragmentTag) {

        ShowHTMLInfoDialogFragment fragment = ShowHTMLInfoDialogFragment.newInstance(urlInfo);
        fragment.show(getSupportFragmentManager(), fragmentTag);
    }


    public static void createOrder(Context context) {
        Intent intent = new Intent(context, OrderEditActivity.class);
        context.startActivity(intent);
    }

    public static void editOrder(long orderMixedId, Context context) {
        Intent intent = new Intent(context, OrderEditActivity.class);
        intent.putExtra(EXTRA_ORDER_ID, orderMixedId);
        context.startActivity(intent);
    }


    @Override
    public void onWorkStartOptionChoosen(String ftag, Integer option, Date from, Date to) {
        selectedDateStart = from;
        selectedDateFinish = to;
        startDateOption = option;

        if (startDateOption == WorkStartOptionChooserDialogFragment.CHOOSE_START) {
            workStartOptionsChooser.setText(getString(R.string.work_start_dialog_no_later_than_date) + " " + StringUtils.convertDatetoString(selectedDateStart));
        } else if (startDateOption == WorkStartOptionChooserDialogFragment.CHOOSE_PERIOD) {
            workStartOptionsChooser.setText(getString(R.string.from) + " " + StringUtils.convertDatetoString(selectedDateStart)
                    + " " + getString(R.string.to) + " " + StringUtils.convertDatetoString(selectedDateFinish));
        } else {
            workStartOptionsChooser.setText(getString(R.string.begin_work_any_time));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {

                Uri uri = data.getData();
                pictures.add(uri);
                imagesAdapter.setPictures(pictures);
            } else if (requestCode == MapActivityExt.REQUEST_CODE_PICK_GEO_POSITION) {
                locationName = data.getStringExtra(MapActivityExt.EXTRA_POINT_NAME);
                GeoPoint geoPoint = data.getParcelableExtra(MapActivityExt.EXTRA_GEOPOINT);
                settings.setLocation(locationName, geoPoint.getLon(), geoPoint.getLat());
                locationLatitude = GeoUtils.formatCoord(geoPoint.getLat());
                locationLongitude = GeoUtils.formatCoord(geoPoint.getLon());
                locationNameTextView.setText(locationName);
            }
        }
    }

    @Override
    public void onDeleteAccepted(String ftag, Uri uri) {
        pictures.remove(uri);
        imagesAdapter.setPictures(pictures);
    }

    private static class ImagesAdapter extends BaseAdapter {

        private LayoutInflater inflater;

        private List<Uri> pictures = Collections.emptyList();

        private OnClickListener listener;

        public void setListener(OnClickListener listener) {
            this.listener = listener;
        }

        public void setPictures(List<Uri> pictures) {
            this.pictures = pictures;
            notifyDataSetChanged();
        }

        public ImagesAdapter(LayoutInflater inflater) {
            this.inflater = inflater;
        }

        @Override
        public int getCount() {
            return pictures.size();
        }

        @Override
        public Uri getItem(int i) {
            return pictures.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            final ViewHolder holder;
            if (view == null) {
                holder = new ViewHolder();
                view = inflater.inflate(R.layout.order_edit_image, viewGroup, false);
                holder.imgAttach = (ImageView) view.findViewById(R.id.order_image);
                holder.deleteAttachFileButton = (ImageView) view.findViewById(R.id.delete_attach_file);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }

            holder.deleteAttachFileButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(pictures.get(i));
                }
            });

            Picasso.with(view.getContext()).load(pictures.get(i)).into(holder.imgAttach);
            return view;
        }

        public class ViewHolder {
            ImageView imgAttach;
            ImageView deleteAttachFileButton;
        }

        public static interface OnClickListener {
            public void onClick(Uri pictureItem);
        }

    }

    private void setSeekBarBudgetValue() {
        if (orderMixedItem != null) {
            long budgetId = orderMixedItem.getBudgetCategory().getId();
            for (int i = 0; i < budgetCategories.size(); i++) {
                if (budgetId == budgetCategories.get(i).getId()) {
                    seekBarBudget.setProgress(i);
                    break;
                }
            }
        }
    }

    private void showOrHideUnregisteredFieldsPart() {
        isLoggedIn = settings.isLoggedIn();
        unregisteredFieldsPart = (LinearLayout) findViewById(R.id.unregistered_fields_part);
        if (!isLoggedIn) {
            unregisteredFieldsPart.setVisibility(View.VISIBLE);
        } else {
            unregisteredFieldsPart.setVisibility(View.GONE);
        }
    }

    private void sendOrder() {
        boolean valid = validate();

        if (!valid) {
            return;
        }

        CreateOrUpdateOrderOperation.Params params = new CreateOrUpdateOrderOperation.Params();
        params.setTitle(StringUtils.getTextFromView(orderTitle));
        params.setDetails(StringUtils.getTextFromView(orderDescription));
        params.setAddress(locationName);
        params.setLatLon(locationLatitude, locationLongitude);
        params.setBudgetCategory(choosenBudgetCategoryId);
        params.setOrderPictures(pictures.get(0).getPath());
        params.setStartDates(startDateOption, selectedDateStart, selectedDateFinish);
        params.setOsmId(102269); //TODO set osm_id

        if (!isLoggedIn) {
            params.setUserEmail("fake" + StringUtils.getTextFromView(orderPhone) + "@grr.la");//TODO email change to phone
            params.setUserName(StringUtils.getTextFromView(orderContactInfo));
        }
        if (orderMixedId != None.NONE_LONG) params.setOrderId(orderMixedId);
        mClient.createOrUpdateOrderAsync(params);
    }

    @Subscribe
    public void CreateOrUpdateOrderSuccess(mClient.CreateOrUpdateOrderSuccess event) {
        Toast.makeText(this, "success", Toast.LENGTH_LONG).show();
    }

    @Subscribe
    public void CreateOrUpdateOrderFailed(mClient.CreateOrUpdateOrderFailed event) {
        Toast.makeText(this, "error", Toast.LENGTH_LONG).show();
    }

    @Subscribe
    public void onBudgetCategorySuccess(mClient.onBudgetCategorySuccess event) {
        Timber.v(TAG, "orderLoaded: BudgetCategory");

        progressBar.setVisibility(View.GONE);
        scrollview.setVisibility(View.VISIBLE);

        budgetCategories = db.getBudgetCategories();
        enableSeekBarChangeListener();
    }

    @Subscribe
    public void onBudgetCategoryFailed(mClient.onBudgetCategoryFailed event) {
        progressBar.setVisibility(View.GONE);
        retryPart.setVisibility(View.VISIBLE);

        Timber.e(TAG, "orderLoadingFailed: Budget Category");
        dtoast("orderLoadingFailed: Budget Category");
    }


    public boolean validate() {
        try {

            ValidationUtils.validateNotEmpty(orderTitle, scrollview, handler, R.id.content);
            ValidationUtils.validateNotEmpty(orderDescription, scrollview, handler, R.id.content);

            validateWorkStartCondition();
            if (startDateOption == WorkStartOptionChooserDialogFragment.CHOOSE_PERIOD) {
                validateWorkStartPeriod();
            } else if (startDateOption == WorkStartOptionChooserDialogFragment.CHOOSE_START) {
                validateWorkStart();
            }
            validateBudget();
            validateLocation();

            if (!isLoggedIn) {
                ValidationUtils.validateNotEmpty(orderContactInfo, scrollview, handler, R.id.content);
                ValidationUtils.validateNotEmpty(orderPhone, scrollview, handler, R.id.content);

                if (!termsConditional.isChecked()) {
                    toast(R.string.error_termsandconditions);
                    return false;
                }
            }

        } catch (ValidationException ve) {
            Timber.e(ve, "validation failed ");
            return false;
        }

        return true;
    }

    private void validateLocation() {
        if (locationName == null) {
            ValidationUtils.focusOnView(locationNameTextView, scrollview, handler, R.id.content);
            toast(R.string.validation_select_location);
            throw new ValidationException("location not selected");
        }

    }

    private void validateBudget() {
        if (choosenBudgetCategoryId == null) {
            ValidationUtils.focusOnView(orderBudget, scrollview, handler, R.id.content);
            toast(R.string.validation_budget_category);
            throw new ValidationException("buget not selected");
        }
    }

    private void validateWorkStartCondition() {
        if (startDateOption == None.NONE_LONG) {
            ValidationUtils.focusOnView(workStartOptionsChooser, scrollview, handler, R.id.content);
            toast(R.string.validation_work_start_dates);
            throw new ValidationException("start condition not selected");
        }

    }

    private void validateWorkStartPeriod() {
        if (selectedDateStart == null || selectedDateFinish == null) {
            ValidationUtils.focusOnView(workStartOptionsChooser, scrollview, handler, R.id.content);
            toast(R.string.validation_work_start_dates);
            throw new ValidationException("start or finish date not selected");
        }

    }

    private void validateWorkStart() {
        if (startDateOption == None.NONE_LONG) {

            ValidationUtils.focusOnView(workStartOptionsChooser, scrollview, handler, R.id.content);
            toast(R.string.validation_work_start_dates);
            throw new ValidationException("start date not selected");
        }

    }

    private void prefill() {
        orderTitle.setText(orderMixedItem.getTitle());
        orderDescription.setText(orderMixedItem.getDetails());
        orderBudget.setText(orderMixedItem.getBudgetCategory().getTitle());
        orderBudgetDescription.setText(orderMixedItem.getBudgetCategory().getDetails());
        locationNameTextView.setText(orderMixedItem.getAddress());
        locationName = orderMixedItem.getAddress();
        locationLongitude = GeoUtils.formatCoord(orderMixedItem.getLongitude());
        locationLatitude = GeoUtils.formatCoord(orderMixedItem.getLattitude());
        workStartOptionsChooser.setText(FormatUtils.formatWorkBeginTime(orderMixedItem, this));
        selectedDateStart = orderMixedItem.getStartDate();
        selectedDateFinish = orderMixedItem.getEndDate();
        startDateOption = orderMixedItem.getStartDateOption();
    }

    @Override
    protected void onDestroy() {
        bus.unregister(this);
        super.onDestroy();
    }

    private SeekBar.OnSeekBarChangeListener budgetSeekBarListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            BudgetCategoryItem budgetCategoryItem = budgetCategories.get(progress);
            String titleBudget = budgetCategoryItem.getTitle();
            orderBudget.setText(titleBudget);

            String descriptionBudget = budgetCategoryItem.getDetails();
            orderBudgetDescription.setText(descriptionBudget);

            choosenBudgetCategoryId = budgetCategoryItem.getId();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };

    private void enableSeekBarChangeListener() {
        seekBarBudget.setMax(budgetCategories.size() - 1);
        seekBarBudget.setOnSeekBarChangeListener(budgetSeekBarListener);
        setSeekBarBudgetValue();
    }

    private void disableSeekBarChangeListener() {
        seekBarBudget.setOnSeekBarChangeListener(null);
    }
}
