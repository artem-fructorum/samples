package com.fructorum.sample.sample1;


import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.fructorum.sample.R;

public class AttachFileDeleteDialogFragment extends DialogFragment {

    private static final String TAG = "AttachFileDeleteDialogFrag";

    private Uri uriAttachFile;

    private static final String KEY_URI = "uri";


    public static AttachFileDeleteDialogFragment newInstance(Uri uriAttachFile) {
        Bundle args = new Bundle();
        args.putString(KEY_URI, uriAttachFile.toString());
        AttachFileDeleteDialogFragment attachFileDeleteDialogFragment = new AttachFileDeleteDialogFragment();
        attachFileDeleteDialogFragment.setArguments(args);
        return attachFileDeleteDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.mDialogTheme);

        Bundle bundle = this.getArguments();
        String strUri = bundle.getString(KEY_URI);
        uriAttachFile = Uri.parse(strUri);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filter_delete_dialog, container, false);

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(getString(R.string.delete_file));

        Button noDontDeleteBtn = (Button) view.findViewById(R.id.no_dont_delete);
        Button yesDeleteBtn = (Button) view.findViewById(R.id.yes_delete);

        noDontDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        yesDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Listener) getActivity()).onDeleteAccepted(getTag(), uriAttachFile);
                dismiss();
            }
        });


        return view;
    }

    public static interface Listener {
        void onDeleteAccepted(String ftag, Uri uri);
    }

}
