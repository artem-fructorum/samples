package com.fructorum.sample.sample1;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.fructorum.sample.R;

import java.util.Calendar;
import java.util.Date;

public class SingleDateChooserFragment  extends CaldroidFragment {

    private Date selectedDate = null;
    private Date prevDate = null;


    public static SingleDateChooserFragment newInstance(Date selectedDate) {
        SingleDateChooserFragment fragment = new SingleDateChooserFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, false);
        fragment.setMinDate(cal.getTime());
        fragment.setArguments(args);

        fragment.selectedDate = selectedDate;

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setCaldroidListener(new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {

                if (prevDate != null) {
                    setBackgroundResourceForDate(R.color.unselected_day_color_bg, prevDate);
                }
                selectedDate = date;
                setBackgroundResourceForDate(R.color.selected_day_color_bg, date);
                prevDate = date;
                refreshView();
            }
        });
    }

    private void refreshDateStyles() {
        if (selectedDate != null) {
            prevDate = selectedDate;
            setBackgroundResourceForDate(R.color.selected_day_color_bg, selectedDate);
            refreshView();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        LinearLayout ll = (LinearLayout) view.getRootView();
        inflater.inflate(R.layout.caldroid_buttons, ll, true);

        View okButton = view.findViewById(R.id.dates_chooser_ok_btn);
        View cancelButton = view.findViewById(R.id.dates_chooser_cancel_btn);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Listener) getParentFragment()).onDateChoosen(getTag(), selectedDate);
                dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        refreshDateStyles();

        return view;
    }

    public interface Listener {

        public void onDateChoosen(String ftag, Date date);

    }
}
