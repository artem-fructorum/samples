package com.fructorum.sample.sample1;


import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ViewUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.fructorum.sample.R;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class BaseActivity extends AppCompatActivity {


    private AlertDialog secretDialog;
    private int logoClickTimes = 0;
    private final int SECRET_DIALOG_CLICK_TIMES = 10;
    private final int SECRET_DIALOG_PERIOD = 10000;
    private long lastTimeLogoClicked = 0;

    private static final String TAG = "BaseActivity";

    private void toast(final CharSequence text, final int length) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(BaseActivity.this, text, length).show();
            }
        });
    }

    public void toast(final CharSequence text) {
        toast(text, Toast.LENGTH_SHORT);
    }

    public void longToast(final CharSequence text) {
        toast(text, Toast.LENGTH_LONG);
    }


    public void dtoast(final CharSequence text) {
        if (Config.CONFIG.DTOASTS_ENABLED()) {
            toast(text);
        }
    }

    public void toast(final @StringRes int text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(BaseActivity.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public mApplication getApp() {
        return (mApplication) getApplication();
    }

    protected void tintIcon(MenuItem item, @ColorRes int colorRes) {
        ViewUtils.tintMenuItem(this, item, colorRes);
    }

    private int getOrientation() {
        return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        updateGTM();
        setRequestedOrientation(getOrientation());

        try {
            setDisplayHomeAsUp();
        } catch (Exception e) {
            Timber.v("error getting actionbar (ignore if you have NoActionBarTheme)");
        }
    }

    public void setDisplayHomeAsUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    private void updateGTM() {
        TagManager tagManager = TagManager.getInstance(getApplicationContext());
        final String gtmContainerId = Config.CONFIG.GTM_CONFIG().containerId;
        final int gtmContainerFileRes = Config.CONFIG.GTM_CONFIG().containerFileRaw;

        PendingResult<ContainerHolder> pending = tagManager.loadContainerPreferNonDefault(gtmContainerId, gtmContainerFileRes);
        pending.setResultCallback(new ResultCallback<ContainerHolder>() {
            @Override
            public void onResult(ContainerHolder containerHolder) {
                if (!containerHolder.getStatus().isSuccess()) {
                    Timber.e("gtm container loading failed " + gtmContainerId);
                    return;
                }
                ContainerHolderSingleton.setContainerHolder(containerHolder);
            }
        }, 2, TimeUnit.SECONDS);
    }

    @Override
    protected void onStart() {
        super.onStart();
        GTM.pushOpenScreenEvent(this);
        FlurryAgent.onStartSession(this, Config.CONFIG.FLURRY_KEY());
    }

    @Override
    protected void onStop() {
        super.onStop();
        GTM.pushCloseScreenEvent(this);
        FlurryAgent.onEndSession(this);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (android.R.id.home == item.getItemId()) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    void exitApp(Config config, String data, AlertDialog secretDialog) {
        Settings settings = getApp().getSettings();
        settings.setConfigName(data);
        Config.setConfig(config);
        getApp().logout();
        secretDialog.dismiss();
        finish();
        System.exit(0);
    }

    public void createClickerForLogo(View secretLogo) {
        secretLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastTimeLogoClicked == 0) {
                    lastTimeLogoClicked = System.currentTimeMillis();
                    logoClickTimes++;
                } else {
                    if (lastTimeLogoClicked + SECRET_DIALOG_PERIOD
                            > lastTimeLogoClicked - System.currentTimeMillis()) {
                        if (logoClickTimes == SECRET_DIALOG_CLICK_TIMES) {
                            openSecretDialog();
                        } else {
                            logoClickTimes++;
                        }

                    } else {
                        lastTimeLogoClicked = System.currentTimeMillis();
                        logoClickTimes = 0;
                    }

                }
            }
        });
    }

    public void openSecretDialog() {
        if (secretDialog == null || !secretDialog.isShowing()) {
            AlertDialog.Builder secretDialogBuilder;
            View v = getLayoutInflater().inflate(R.layout.dialog_change_config, null);
            secretDialogBuilder = new AlertDialog.Builder(this);
            secretDialogBuilder.setView(v);
            secretDialog = secretDialogBuilder.create();
            secretDialog.show();
            v.findViewById(R.id.release).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    exitApp(new ReleaseConfig(), "release", secretDialog);

                }
            });
            v.findViewById(R.id.stagingConfig).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    exitApp(new StagingConfig(), "staging", secretDialog);
                }
            });
            v.findViewById(R.id.startLog).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getApp().getSettings().setTimberLogState(1);
                    getApp().getBufferTimber().setTimberLogState(0);
                    secretDialog.dismiss();

                }
            });
            v.findViewById(R.id.stopLog).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getApp().getSettings().setTimberLogState(1);
                    getApp().getBufferTimber().setTimberLogState(0);
                    secretDialog.dismiss();
                }
            });
            v.findViewById(R.id.shareLog).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    secretDialog.dismiss();
                    ArrayList<Uri> uris = new ArrayList<Uri>();
                    BufferTimber bufferTimber = getApp().getBufferTimber();
                    File folder = bufferTimber.getFolder();
                    if (folder != null) {
                        for (File file : folder.listFiles()) {
                            if (file.getName().endsWith(".log")) {
                                uris.add(Uri.fromFile(file));
                            }
                        }
                    }

                    final Intent shareIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
                    shareIntent.setType("plain/text");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Logs");
                    shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
                    startActivity(Intent.createChooser(shareIntent, "Sending multiple attachment"));
                }
            });
            v.findViewById(R.id.deleteLog).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getApp().getBufferTimber().cleanUp();
                    secretDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PhoneUtils.PERMISSION_PHONE_CALLS_REQUEST_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grant = grantResults[i];

                if (PhoneUtils.PERMISSION_PHONE_CALLS.equals(permission)) {
                    if (grant == PackageManager.PERMISSION_GRANTED) {
                        Log.i(TAG, permission + " granted");
                    } else if (grant == PackageManager.PERMISSION_DENIED) {
                        Log.i(TAG, permission + " denied");
                        toast(R.string.grant_phone_call_permission);
                    }
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}